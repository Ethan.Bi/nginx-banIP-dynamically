local banDuration    = 7200   --ban timeout, in sec
local countInterval  = 60     --time interval for checking number of access, in sec
local numberOfAccess = 100    --the max number of access is allowed in interval of countInterval

--connect to redis
local redis = require "resty.redis"
local cache = redis:new()
cache:set_timeout(500)  -- connection timeout 500ms
local ok , err = cache:connect("127.0.0.1", "6379")

if not ok then
    ngx.log(ngx.ERR, "Failed to connect to redis.", err)
    cache:close()
    return
end

local ip = ngx.req.get_headers()["X-Real-IP"]
if ip == nil then
    ip = ngx.req.get_headers()["x_forwarded_for"]
end
if ip == nil then
    ip = ngx.var.remote_addr
end

if ip == nil then
    ngx.log(ngx.ERR, "Unexpected request with no remote ip has been found!")
    ngx.exit(403)
    cache:close()
    return
end

local record, err = cache:get(ip)
if record == ngx.null then
    cache:set(ip, 1)                --init record from the number of 1
    cache:expire(ip, countInterval) --set expiration time, to make sure it can be removed if its a normal access
    cache:close()
    return
end

record = tonumber(record);
if record < numberOfAccess then
    cache:incrby(ip, 1)
elseif record == numberOfAccess then
    cache:incrby(ip, 1)           --make sure it can only hit here once
    cache:expire(ip, banDuration) --do the actual ban operation
    ngx.log(ngx.INFO, "ban ip:", ip)
else
    ngx.exit(403)  --the ip is banned, so just return 403 to the client
end

local ok, err = cache:set_keepalive(30000, 1000) --connection pool, number 100, idle time 10 seconds
if not ok then
    ngx.log(ngx.INFO, "Failed to set connection pool.")
    return
end