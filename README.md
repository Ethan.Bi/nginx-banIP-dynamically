# nginx-banIP-dynamically

ban IP dynamically in nginx if an IP making too many hits to your nginx server in a given interval.

#### dependancy:
- [lua-nginx-module/openresty](https://github.com/openresty/lua-nginx-module)
- [lua-resty-redis](https://github.com/openresty/lua-resty-redis)
- [redis](https://redis.io/)

#### usage:
in nginx.conf refer to throttle.lua in any of the following blocks:
- http
- server
- location

#### Example:
```javascript
location / {
    access_by_lua_file /path/to/throttle.lua
}
```

For other information, please check the comments in the code.